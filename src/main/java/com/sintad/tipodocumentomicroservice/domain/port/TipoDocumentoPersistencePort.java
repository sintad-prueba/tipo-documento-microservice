package com.sintad.tipodocumentomicroservice.domain.port;

import com.sintad.tipodocumentomicroservice.domain.model.TipoDocumento;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
public interface TipoDocumentoPersistencePort {
    Mono<TipoDocumento> save (TipoDocumento model);
    Mono<TipoDocumento> update (TipoDocumento model);
    Mono<Void> deleteById (Long id);
    Flux<TipoDocumento> findAll();
}
