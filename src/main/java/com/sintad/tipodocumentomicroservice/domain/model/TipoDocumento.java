package com.sintad.tipodocumentomicroservice.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author roberth
 */
@Getter
@Setter
@Builder
public class TipoDocumento {

    private Long idTipoDocumento;
    private String codigo;
    private String nombre;
    private String descripcion;
    private boolean estado;
}
