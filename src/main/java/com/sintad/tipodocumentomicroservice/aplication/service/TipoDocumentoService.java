package com.sintad.tipodocumentomicroservice.aplication.service;

import com.sintad.tipodocumentomicroservice.aplication.usecase.TipoDocumentoUseCases;
import com.sintad.tipodocumentomicroservice.domain.model.TipoDocumento;
import com.sintad.tipodocumentomicroservice.domain.port.TipoDocumentoPersistencePort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Service
@AllArgsConstructor
public class TipoDocumentoService implements  TipoDocumentoUseCases{
    
    TipoDocumentoPersistencePort persistence;

    @Override
    public Flux<TipoDocumento> findAll() {
        return persistence.findAll();
    }

    @Override
    public Mono<TipoDocumento> save(TipoDocumento model) {
        return persistence.save(model);
    }

    @Override
    public Mono<TipoDocumento> update(TipoDocumento model) {
        return persistence.update(model);
    }

    @Override
    public Mono<Void> deleteById(Long id) {
        return persistence.deleteById(id);
    }
    
}
