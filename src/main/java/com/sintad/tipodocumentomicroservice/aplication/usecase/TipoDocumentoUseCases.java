
package com.sintad.tipodocumentomicroservice.aplication.usecase;

import com.sintad.tipodocumentomicroservice.domain.model.TipoDocumento;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
public interface TipoDocumentoUseCases {
    
    Flux<TipoDocumento> findAll();
    Mono<TipoDocumento> save (TipoDocumento model);
    Mono<TipoDocumento> update (TipoDocumento model);
    Mono<Void> deleteById (Long id);
}
