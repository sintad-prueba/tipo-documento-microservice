package com.sintad.tipodocumentomicroservice.infraestructure.rest.handler;

import com.sintad.tipodocumentomicroservice.aplication.service.TipoDocumentoService;
import com.sintad.tipodocumentomicroservice.domain.model.TipoDocumento;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Component
@AllArgsConstructor
public class TipoDocumentoHandler {
    
    private final MediaType typeJson = MediaType.APPLICATION_JSON;
    
    TipoDocumentoService service;
    
    public Mono<ServerResponse> save(ServerRequest request){
        Mono<TipoDocumento> tipoDocumento = request.bodyToMono(TipoDocumento.class);
        
        return tipoDocumento.flatMap(model -> ServerResponse.ok()
                .contentType(typeJson)
                .body(service.save(model), TipoDocumento.class)
        );
    }
    
    public Mono<ServerResponse> update(ServerRequest request){
        Mono<TipoDocumento> tipoDocumento = request.bodyToMono(TipoDocumento.class);
        
        return tipoDocumento.flatMap(model -> ServerResponse.ok()
                .contentType(typeJson)
                .body(service.update(model), TipoDocumento.class)
        );
    }
    
    public Mono<ServerResponse> deleteById(ServerRequest request){
        Long id = Long.valueOf(request.pathVariable("id"));
        
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(service.deleteById(id), TipoDocumento.class);
    }
    
    public Mono<ServerResponse> findAll(ServerRequest request){
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(service.findAll(), TipoDocumento.class);
    }
    
}
