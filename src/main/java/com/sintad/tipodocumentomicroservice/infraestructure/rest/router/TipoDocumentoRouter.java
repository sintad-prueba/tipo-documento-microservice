package com.sintad.tipodocumentomicroservice.infraestructure.rest.router;

import com.sintad.tipodocumentomicroservice.infraestructure.rest.handler.TipoDocumentoHandler;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 *
 * @author roberth
 */
@Configuration
public class TipoDocumentoRouter {

    private static String path = "/api/tipo-documento";

    @Bean
    public WebProperties.Resources resources() {
        return new WebProperties.Resources();
    }

    @Bean
    RouterFunction<ServerResponse> routerTipoDocumento(TipoDocumentoHandler handler) {
        return RouterFunctions.route()
                .GET(path, handler::findAll)
                .POST(path, handler::save)
                .PUT(path, handler::update)
                .DELETE(path + "/{id}", handler::deleteById)
                .build();
    }

}
