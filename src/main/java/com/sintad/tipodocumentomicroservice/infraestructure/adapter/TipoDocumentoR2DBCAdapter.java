package com.sintad.tipodocumentomicroservice.infraestructure.adapter;

import com.sintad.tipodocumentomicroservice.domain.model.TipoDocumento;
import com.sintad.tipodocumentomicroservice.domain.port.TipoDocumentoPersistencePort;
import com.sintad.tipodocumentomicroservice.infraestructure.adapter.entity.TipoDocumentoEntity;
import com.sintad.tipodocumentomicroservice.infraestructure.adapter.repository.TipoDocumentoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Service
@AllArgsConstructor
public class TipoDocumentoR2DBCAdapter implements TipoDocumentoPersistencePort {

    TipoDocumentoRepository repository;
    
    @Override
    public Mono<TipoDocumento> save(TipoDocumento model) {
        return repository.save(TipoDocumentoEntity.builder().build().fromDomain(model))
                .map(entity -> entity.toDomain(entity));
    }

    @Override
    public Mono<TipoDocumento> update(TipoDocumento model) {
         return repository.save(TipoDocumentoEntity.builder().build().fromDomain(model))
                .map(entity -> entity.toDomain(entity));
    }

    @Override
    public Mono<Void> deleteById(Long id) {
        return repository.deleteById(id);
    }

    @Override
    public Flux<TipoDocumento> findAll() {
       return repository.findAll()
               .map(entity -> entity.toDomain(entity));
    }
    
}
