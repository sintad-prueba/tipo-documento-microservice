package com.sintad.tipodocumentomicroservice.infraestructure.adapter.repository;

import com.sintad.tipodocumentomicroservice.infraestructure.adapter.entity.TipoDocumentoEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberth
 */
@Repository
public interface TipoDocumentoRepository extends ReactiveCrudRepository<TipoDocumentoEntity, Long>{
    
}
