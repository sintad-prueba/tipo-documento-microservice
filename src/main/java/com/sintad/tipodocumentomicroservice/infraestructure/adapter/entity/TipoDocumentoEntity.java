/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sintad.tipodocumentomicroservice.infraestructure.adapter.entity;

import com.sintad.tipodocumentomicroservice.domain.model.TipoDocumento;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 *
 * @author roberth
 */
@Table(value = "tb_tipo_documento")
@Getter
@Setter
@Builder
public class TipoDocumentoEntity {

    @Id
    private Long idTipoDocumento;
    private String codigo;
    private String nombre;
    private String descripcion;
    private boolean estado;

    public TipoDocumento toDomain(TipoDocumentoEntity entity) {
        return TipoDocumento.builder()
                .idTipoDocumento(entity.getIdTipoDocumento())
                .codigo(entity.getCodigo())
                .nombre(entity.getNombre())
                .descripcion(entity.getDescripcion())
                .estado(entity.isEstado())
                .build();
    }

    public TipoDocumentoEntity fromDomain(TipoDocumento model) {
        return TipoDocumentoEntity.builder()
                .idTipoDocumento(model.getIdTipoDocumento())
                .codigo(model.getCodigo())
                .nombre(model.getNombre())
                .descripcion(model.getDescripcion())
                .estado(model.isEstado())
                .build();
    }

}
