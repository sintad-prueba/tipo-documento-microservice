package com.sintad.tipodocumentomicroservice.infraestructure.config;

import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;

/**
 *
 * @author roberth
 */
@Configuration
public class MySqlR2DBCConfig extends AbstractR2dbcConfiguration {
    
    @Value("${spring.r2dbc.url}")
    private String mysqlDBUrl;


    @Override
    public ConnectionFactory connectionFactory() {
        return ConnectionFactories.get(mysqlDBUrl);
    }
    
}
