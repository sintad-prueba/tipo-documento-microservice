package com.sintad.tipodocumentomicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TipoDocumentoMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TipoDocumentoMicroserviceApplication.class, args);
	}

}
